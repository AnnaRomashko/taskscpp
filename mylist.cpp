#include "mylist.h"

template<typename DataType>
MyList<DataType>::MyList()
{
    head=nullptr;
    tail=nullptr;
    length=0;
    error="OK";
}

template<typename DataType>
MyList<DataType>::MyList(DataType data)
{
    MyListElem<DataType> *elem = new MyListElem<DataType>;
    elem->myListData=data;
    head=elem;
    tail=elem;
    length=1;
    error="OK";
}

template<typename DataType>
MyListElem<DataType>* MyList<DataType>::find_elem_by_data(DataType data)
{
    this->error="no such element";
    if (this->length == 0) return head;
    MyListElem<DataType> *elem = this->head;
    for (int i=1; i<=this->length;i++)
    {
        if (elem->myListData == data)
        {
            this->error="OK";
            return elem;
        }
        elem=elem->Right;
    }
    return head;
}

template<typename DataType>
MyListElem<DataType>* MyList<DataType>::find_elem_by_index(int index)
{
    this->error="no such element";
    if (this->length == 0) return head;
    if (index< this->length/2)
    {
        MyListElem<DataType> *elem = this->head;
        for (int i=1; i<index;i++)
            elem=elem->Right;
        this->error="OK";
        return elem;
    }
    else
    {
        MyListElem<DataType> *elem = this->tail;
        for (int i=this->length; i>index;i--)
            elem=elem->Left;
        this->error="OK";
        return elem;
    }
}

template<typename DataType>
void MyList<DataType>::add_elem_to_beginning(DataType data)
{
    this->error="OK";
    MyListElem<DataType> *newElem = new MyListElem<DataType>;
    newElem->myListData=data;
    if (this->length == 0)
    {
        this->head=newElem;
        this->tail=newElem;
        this->length=1;
    }
    else
    {
        MyListElem<DataType> *tempElem;
        newElem->Right=this->head;
        tempElem=this->head;
        tempElem->Left=newElem;
        this->head=newElem;
        this->length++;
    }
}

template<typename DataType>
void MyList<DataType>::add_elem_to_end(DataType data)
{
    this->error="OK";
    MyListElem<DataType> *newElem = new MyListElem<DataType>;
    newElem->myListData=data;
    if (this->length == 0)
    {
        this->head=newElem;
        this->tail=newElem;
        this->length=1;
    }
    else
    {
        MyListElem<DataType> *tempElem;
        newElem->Left=this->tail;
        tempElem=this->tail;
        tempElem->Right=newElem;
        this->tail=newElem;
        this->length++;
    }
}

template<typename DataType>
void MyList<DataType>::add_elem_to_index(DataType data, int index)
{
    if (index<1 || index > this->length + 1)
    {
        this->error="no such index";
        return;
    }
    this->error="OK";
    MyListElem<DataType> *newElem = new MyListElem<DataType>;
    newElem->myListData=data;
    if (this->length == 0)
    {
        this->head=newElem;
        this->tail=newElem;
        this->length=1;
    }
    else if (index == 1)
    {
        this->add_elem_to_beginning(data);
    }
    else if (index > this->length)
    {
        this->add_elem_to_end(data);
    }
    else
    {
        MyListElem<DataType> *tempElem;
        tempElem = this->find_elem_by_index(index);
        MyListElem<DataType> *leftElem = tempElem->Left;
        newElem->Left=leftElem;
        newElem->Right=tempElem;
        tempElem->Left=newElem;
        leftElem->Right=newElem;
        this->length++;
    }
}

template<typename DataType>
void MyList<DataType>::delete_elem_by_data(DataType data)
{
    MyListElem<DataType> *newElem = this->find_elem_by_data(data);
    if (this->error == "no such element")
    {
        this->error = "unable to delete non-existent element";
        return;
    }
    this->error="OK";
    while (this->error != "no such element")
    {
        if (this->length == 1)
        {
            newElem=this->head;
            delete newElem;
            this->head=nullptr;
            this->tail=nullptr;
            this->length=0;
        }
        else if (newElem->Right != nullptr && newElem->Left != nullptr)
        {
            MyListElem<DataType> *rightElem = newElem->Right;
            MyListElem<DataType> *leftElem = newElem->Left;
            rightElem->Left=leftElem;
            leftElem->Right=rightElem;
            this->length--;
        }
        else if(newElem->Right != nullptr)
        {
            this->head=newElem->Right;
            this->length--;
        }
        else if (newElem->Left != nullptr)
        {
            this->tail=newElem->Left;
            this->length--;
        }
        else this->error="something went wrong while delete element";
        newElem = this->find_elem_by_data(data);
    }
    this->error="OK";
}

template<typename DataType>
void MyList<DataType>::delete_elem_by_index(int index)
{
    if (index<1 || index > this->length)
    {
        this->error="no such index";
        return;
    }
    MyListElem<DataType> *newElem = this->find_elem_by_index(index);
    this->error="OK";
    if (this->length == 1)
    {
        newElem=this->head;
        delete newElem;
        this->head=nullptr;
        this->tail=nullptr;
        this->length=0;
    }
    else if (newElem->Right != nullptr && newElem->Left != nullptr)
    {
        MyListElem<DataType> *rightElem = newElem->Right;
        MyListElem<DataType> *leftElem = newElem->Left;
        rightElem->Left=leftElem;
        leftElem->Right=rightElem;
        this->length--;
    }
    else if(newElem->Right != nullptr)
    {
        this->head=newElem->Right;
        this->length--;
    }
    else if (newElem->Left != nullptr)
    {
        this->tail=newElem->Left;
        this->length--;
    }
    else this->error="something went wrong while delete element";
}

template<typename DataType>
void MyList<DataType>::swap_elems_by_data(DataType data1, DataType data2)
{
    MyListElem<DataType> *firstElem = this->find_elem_by_data(data1);
    if (this->error == "no such element")
    {
        this->error = "first element doesnt exist";
        return;
    }
    MyListElem<DataType> *secondElem = this->find_elem_by_data(data2);
    if (this->error == "no such element")
    {
        this->error = "second element doesnt exist";
        return;
    }
    firstElem->myListData=data2;
    secondElem->myListData=data1;
}

template<typename DataType>
void MyList<DataType>::swap_elems_by_index(int index1, int index2)
{
    if (index1<1 || index1 > this->length)
    {
        this->error="first index doesnt exist";
        return;
    }
    if (index2<1 || index2 > this->length)
    {
        this->error="second index doesnt exist";
        return;
    }
    MyListElem<DataType> *firstElem = this->find_elem_by_index(index1);
    MyListElem<DataType> *secondElem = this->find_elem_by_index(index2);
    DataType data=firstElem->myListData;
    firstElem->myListData=secondElem->myListData;
    secondElem->myListData=data;
}

template<typename DataType>
void MyList<DataType>::sort_list(TypeSort typeSort)
{
    if (this->length < 2) return;
    MyListElem<DataType> *thisElem = this->head;
    MyListElem<DataType> *rightElem = thisElem->Right;
    switch (typeSort)
    {
    case up:
    {
        for (int i=1; i<=this->length;i++)
        {
            thisElem = this->head;
            for(int j=1; j<=this->length-i; j++)
            {
                rightElem=thisElem->Right;
                if (thisElem->myListData > rightElem->myListData)
                {
                    this->swap_elems_by_index(j,j+1);
                }
                thisElem=rightElem;
            }
        }
        break;
    }
    case down:
    {
        for (int i=1; i<=this->length;i++)
        {
            thisElem = this->head;
            for(int j=1; j<=this->length-i; j++)
            {
                rightElem=thisElem->Right;
                if (thisElem->myListData < rightElem->myListData)
                {
                    this->swap_elems_by_index(j,j+1);
                }
                thisElem=rightElem;
            }
        }
        break;
    }
    default:
    {
        this->error = "something wrong with sort type. List was sorted with sort type up";
        this->sort_list(up);
    }
    }
}

template<typename DataType>
void MyList<DataType>::print_list()
{
    if (this->length == 0)
    {
        this->error="list is empty";
        cout << this->error << endl;
        return;
    }
    this->error="OK";
    MyListElem<DataType> *thisElem=this->head;
    for (int i=1; i<=this->length; i++)
    {
        cout << "№ " << i << " "<< "data = " << thisElem->myListData << endl;
        thisElem=thisElem->Right;
    }
    cout<<endl;
}

template<typename DataType>
string MyList<DataType>::print_list_to_string()
{
    string tempString="";
    if (this->length == 0)
    {
        this->error="list is empty";
        tempString=this->error;
        return tempString;
    }
    this->error="OK";
    MyListElem<DataType> *thisElem=this->head;
    for (int i=1; i<=this->length; i++)
    {
        tempString=tempString + "№ " + to_string(i) + " data " + to_string(thisElem->myListData) + "\n";
        thisElem=thisElem->Right;
    }
    return tempString;
}

template<>
string MyList<char>::print_list_to_string()
{
    string tempString="";
    if (this->length == 0)
    {
        this->error="list is empty";
        tempString=this->error;
        return tempString;
    }
    this->error="OK";
    MyListElem<char> *thisElem=this->head;
    for (int i=1; i<=this->length; i++)
    {
        tempString=tempString + "№ " + to_string(i) + " data " + thisElem->myListData + "\n";
        thisElem=thisElem->Right;
    }
    return tempString;
}

template<typename DataType>
string MyList<DataType>::get_error()
{
    return error;
}

template class MyList<int>;
template class MyList<float>;
template class MyList<char>;
