#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>

enum TypeList {intList, floatList, charList}; // типы элементов списка
enum TypeAddElem {beginningList, endList, posList}; // куда добавить элемент в списке
enum TypeDeleteSwapElems {dataList, indexList}; // по данным или индексу удалить/поменять местами элементы в списке

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->comboBoxListOf->addItem("int",intList); // заполнение выпадающего списка ListOf
    ui->comboBoxListOf->addItem("float",floatList);
    ui->comboBoxListOf->addItem("char",charList);

    ui->comboBoxTypeSort->addItem("up", up); // заполнение выпадающего списка TypeSort
    ui->comboBoxTypeSort->addItem("down", down);

    ui->comboBoxWhereAddElem->addItem("beginning", beginningList);  // заполнение выпадающего списка WhereAddElem
    ui->comboBoxWhereAddElem->addItem("end", endList);
    ui->comboBoxWhereAddElem->addItem("position", posList);

    ui->List->hide();
    ui->TypeSort->hide();
    ui->FirstElem->hide();
    ui->SecondElem->hide();
    ui->TypeAddElem->hide();
    ui->scrollAreaList->hide();
    ui->textBrowserList->hide();
    ui->comboBoxTypeSort->hide();
    ui->comboBoxWhereAddElem->hide();
    ui->pushButtonAddElem->hide();
    ui->pushButtonSortList->hide();
    ui->pushButtonPrintList->hide();
    ui->pushButtonSwapElems->hide();
    ui->pushButtonDeleteElem->hide();
    ui->textEditDataFirstElem->hide();
    ui->textEditDataSecondElem->hide();
    ui->scrollAreaWidgetContentsList->hide();
    ui->textEditStatus->setText("wait");

    ui->textBrowserLog->insertPlainText("*Hello\n");
    ui->textBrowserLog->insertPlainText("*choose type of list\n");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::add_int_to_beginning(MyList<int> *list) // добавление элемента в начало списка int
{
    ui->textBrowserLog->insertPlainText("*try to add element to beginning of List\n");
    int tempI = (ui->textEditDataFirstElem->toPlainText()).toInt();
    if (QString::number(tempI) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText()+"\n");
        ui->textEditStatus->setText("error");
        return;
    }
    else
    {
        list->add_elem_to_beginning(tempI);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error())+"\n");
            ui->textEditStatus->setText("error");
            return;
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to beginning of List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::add_float_to_beginning(MyList<float> *list) // добавление элемента в начало списка float
{
    ui->textBrowserLog->setText("*try to add element to beginning of List\n");
    float tempF = (ui->textEditDataFirstElem->toPlainText()).toFloat();
    if (QString::number(tempF) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in float format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText()+"\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->add_elem_to_beginning(tempF);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error())+"\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to beginning of List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::add_char_to_beginning(MyList<char> *list) // добавление элемента в начало списка float
{
    ui->textBrowserLog->insertPlainText("*try to add element to beginning of List\n");
    string tempS=ui->textEditDataFirstElem->toPlainText().toStdString();
    if (tempS.length() > 1)  // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in char format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        char tempC = tempS[0];
        list->add_elem_to_beginning(tempC);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to beginning of List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::add_int_to_end(MyList<int> *list) // добавление элемента в конец списка int
{
    ui->textBrowserLog->insertPlainText("*try to add element to end of List\n");
    int tempI = (ui->textEditDataFirstElem->toPlainText()).toInt();
    if (QString::number(tempI) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->add_elem_to_end(tempI);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to end of List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::add_float_to_end(MyList<float> *list) // добавление элемента в конец списка float
{
    ui->textBrowserLog->insertPlainText("*try to add element to end of List\n");
    float tempF = (ui->textEditDataFirstElem->toPlainText()).toFloat();
    if (QString::number(tempF) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in float format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->add_elem_to_end(tempF);
        if (list->get_error() != "OK")  // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to end of List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::add_char_to_end(MyList<char> *list) // добавление элемента в конец списка char
{
    ui->textBrowserLog->insertPlainText("*try to add element to end of List\n");
    string tempS=ui->textEditDataFirstElem->toPlainText().toStdString();
    if (tempS.length() > 1)  // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in char format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        char tempC = tempS[0];
        list->add_elem_to_end(tempC);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to end of List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::add_int_to_index(MyList<int> *list) // добавление элемента на позицию в списке int
{
    ui->textBrowserLog->insertPlainText("*try to add element to specified position in List\n");
    int tempID = (ui->textEditDataFirstElem->toPlainText()).toInt();
    int tempIP = (ui->textEditDataSecondElem->toPlainText()).toInt();
    if (QString::number(tempID) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (QString::number(tempIP) != ui->textEditDataSecondElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->add_elem_to_index(tempID,tempIP);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to position " + ui->textEditDataSecondElem->toPlainText() + " in List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::add_float_to_index(MyList<float> *list) // добавление элемента на позицию в списке float
{
    ui->textBrowserLog->insertPlainText("*try to add element to specified position in List\n");
    float tempFD = (ui->textEditDataFirstElem->toPlainText()).toFloat();
    int tempIP = (ui->textEditDataSecondElem->toPlainText()).toInt();
    if (QString::number(tempFD) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in float format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (QString::number(tempIP) != ui->textEditDataSecondElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->add_elem_to_index(tempFD,tempIP);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to position " + ui->textEditDataSecondElem->toPlainText() + " in List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::add_char_to_index(MyList<char> *list) // добавление элемента на позицию в списке char
{
    ui->textBrowserLog->insertPlainText("*try to add element to specified position in List\n");
    string tempS=ui->textEditDataFirstElem->toPlainText().toStdString();
    int tempIP = (ui->textEditDataSecondElem->toPlainText()).toInt();
    if (tempS.length() > 1)  // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in char format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (QString::number(tempIP) != ui->textEditDataSecondElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        char tempCD = tempS[0];
        list->add_elem_to_index(tempCD,tempIP);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was added to position " + ui->textEditDataSecondElem->toPlainText() + " in List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::delete_int_data(MyList<int> *list) // удаление элемента по данным из списка int
{
    ui->textBrowserLog->insertPlainText("*try to delete element by data from List\n");
    int tempI = (ui->textEditDataFirstElem->toPlainText()).toInt();
    if (QString::number(tempI) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->delete_elem_by_data(tempI);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was deleted from List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::delete_float_data(MyList<float> *list) // удаление элемента по данным из списка int
{
    ui->textBrowserLog->insertPlainText("*try to delete element by data from List\n");
    float tempF = (ui->textEditDataFirstElem->toPlainText()).toFloat();
    if (QString::number(tempF) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in float format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->delete_elem_by_data(tempF);
        if (list->get_error() != "OK")  // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was deleted from List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::delete_char_data(MyList<char> *list) // удаление элемента по данным из списка int
{
    ui->textBrowserLog->insertPlainText("*try to delete element by data from List\n");
    string tempS=ui->textEditDataFirstElem->toPlainText().toStdString();
    if (tempS.length() > 1)  // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data in char format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        char tempC = tempS[0];
        list->delete_elem_by_data(tempC);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element with data " + ui->textEditDataFirstElem->toPlainText() + " was deleted from List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::delete_int_index(MyList<int> *list) // удаление элемента по позиции из списка int
{
    ui->textBrowserLog->insertPlainText("*try to delete element by index from List\n");
    int tempI = (ui->textEditDataFirstElem->toPlainText()).toInt();
    if (QString::number(tempI) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->delete_elem_by_index(tempI);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element on position " + ui->textEditDataFirstElem->toPlainText() + " was deleted from List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::delete_float_index(MyList<float> *list) // удаление элемента по позиции из списка float
{
    ui->textBrowserLog->insertPlainText("*try to delete element by index from List\n");
    int tempI = (ui->textEditDataFirstElem->toPlainText()).toInt();
    if (QString::number(tempI) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->delete_elem_by_index(tempI);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element on position " + ui->textEditDataFirstElem->toPlainText() + " was deleted from List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::delete_char_index(MyList<char> *list) // удаление элемента по позиции из списка char
{
    ui->textBrowserLog->insertPlainText("*try to delete element by index from List\n");
    int tempI = (ui->textEditDataFirstElem->toPlainText()).toInt();
    if (QString::number(tempI) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->delete_elem_by_index(tempI);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*element on position " + ui->textEditDataFirstElem->toPlainText() + " was deleted from List\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::swap_int_data(MyList<int> *list) // обмен мест элементов по данным списка int
{
    ui->textBrowserLog->insertPlainText("*try to swap elements by data in List\n");
    int tempI1 = (ui->textEditDataFirstElem->toPlainText()).toInt();
    int tempI2 = (ui->textEditDataSecondElem->toPlainText()).toInt();
    if (QString::number(tempI1) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных первого элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data of first element in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input of first element " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (QString::number(tempI2) != ui->textEditDataSecondElem->toPlainText()) // проверка коррекктности входных данных второго элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data of second element in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input of second element " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->swap_elems_by_data(tempI1,tempI2);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*elements with data " + ui->textEditDataFirstElem->toPlainText() + " and data " + ui->textEditDataSecondElem->toPlainText() + " were swapped\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::swap_float_data(MyList<float> *list) // обмен мест элементов по данным списка float
{
    ui->textBrowserLog->insertPlainText("*try to swap elements in List\n");
    float tempF1 = (ui->textEditDataFirstElem->toPlainText()).toFloat();
    float tempF2 = (ui->textEditDataSecondElem->toPlainText()).toFloat();
    if (QString::number(tempF1) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных первого элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data of first element in float format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input of first element " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (QString::number(tempF2) != ui->textEditDataSecondElem->toPlainText()) // проверка коррекктности входных данных первого элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data of second element in float format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input of second element " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->swap_elems_by_data(tempF1,tempF2);
        if (list->get_error() != "OK")  // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*elements with data " + ui->textEditDataFirstElem->toPlainText() + " and data " + ui->textEditDataSecondElem->toPlainText() + " were swapped\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::swap_char_data(MyList<char> *list) // обмен мест элементов по данным списка char
{
    ui->textBrowserLog->insertPlainText("*try to swap elements in List\n");
    string tempS1=ui->textEditDataFirstElem->toPlainText().toStdString();
    string tempS2=ui->textEditDataSecondElem->toPlainText().toStdString();
    if (tempS1.length() > 1) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data of first element in char format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input of first element " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (tempS2.length() > 1) // проверка коррекктности входных данных
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input data of second element in char format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect data input of second element " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        char tempC1 = tempS1[0];
        char tempC2 = tempS2[0];
        list->swap_elems_by_data(tempC1,tempC2);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*elements with data " + ui->textEditDataFirstElem->toPlainText() + " and data " + ui->textEditDataSecondElem->toPlainText() + " were swapped\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::swap_int_index(MyList<int> *list) // обмен мест элементов по позициям списка int
{
    ui->textBrowserLog->insertPlainText("*try to swap elements by index in List\n");
    int tempI1 = (ui->textEditDataFirstElem->toPlainText()).toInt();
    int tempI2 = (ui->textEditDataSecondElem->toPlainText()).toInt();
    if (QString::number(tempI1) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных первого элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index of first element in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input of first element " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (QString::number(tempI2) != ui->textEditDataSecondElem->toPlainText()) // проверка коррекктности входных данных второго элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input intex of second element in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input of second element " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->swap_elems_by_index(tempI1,tempI2);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*elements with index " + ui->textEditDataFirstElem->toPlainText() + " and index " + ui->textEditDataSecondElem->toPlainText() + " were swapped\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::swap_float_index(MyList<float> *list) // обмен мест элементов по позициям списка float
{
    ui->textBrowserLog->insertPlainText("*try to swap elements by index in List\n");
    int tempI1 = (ui->textEditDataFirstElem->toPlainText()).toInt();
    int tempI2 = (ui->textEditDataSecondElem->toPlainText()).toInt();
    if (QString::number(tempI1) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных первого элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index of first element in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input of first element " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (QString::number(tempI2) != ui->textEditDataSecondElem->toPlainText()) // проверка коррекктности входных данных второго элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input intex of second element in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input of second element " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->swap_elems_by_index(tempI1,tempI2);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*elements with index " + ui->textEditDataFirstElem->toPlainText() + " and index " + ui->textEditDataSecondElem->toPlainText() + " were swapped\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::swap_char_index(MyList<char> *list) // обмен мест элементов по позициям списка char
{
    ui->textBrowserLog->insertPlainText("*try to swap elements by index in List\n");
    int tempI1 = (ui->textEditDataFirstElem->toPlainText()).toInt();
    int tempI2 = (ui->textEditDataSecondElem->toPlainText()).toInt();
    if (QString::number(tempI1) != ui->textEditDataFirstElem->toPlainText()) // проверка коррекктности входных данных первого элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input index of first element in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input of first element " + ui->textEditDataFirstElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else if (QString::number(tempI2) != ui->textEditDataSecondElem->toPlainText()) // проверка коррекктности входных данных второго элемента
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Error");
        msgBox.setText("Please input intex of second element in int format");
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*incorrect index input of second element " + ui->textEditDataSecondElem->toPlainText() + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        list->swap_elems_by_index(tempI1,tempI2);
        if (list->get_error() != "OK") // проверка ошибок функции класса MyList
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Warning");
            msgBox.setText(QString::fromStdString(list->get_error()));
            msgBox.exec();
            ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
            ui->textEditStatus->setText("error");
        }
        else
        {
            ui->textBrowserLog->insertPlainText("*elements with index " + ui->textEditDataFirstElem->toPlainText() + " and index " + ui->textEditDataSecondElem->toPlainText() + " were swapped\n");
            ui->textEditStatus->setText("success");
        }
    }
}

void MainWindow::sort_int_up(MyList<int> *list) // сортировка списка int по возрастанию
{
    ui->textBrowserLog->insertPlainText("*try to sort up elements in List\n");
    list->sort_list(up);
    if (list->get_error() != "OK") // проверка ошибок функции класса MyList
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Warning");
        msgBox.setText(QString::fromStdString(list->get_error()));
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*List was sorted up\n");
        ui->textEditStatus->setText("success");
    }
}

void MainWindow::sort_float_up(MyList<float> *list) // сортировка списка float по возрастанию
{
    ui->textBrowserLog->insertPlainText("*try to sort up elements in List\n");
    list->sort_list(up);
    if (list->get_error() != "OK") // проверка ошибок функции класса MyList
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Warning");
        msgBox.setText(QString::fromStdString(list->get_error()));
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*List was sorted up\n");
        ui->textEditStatus->setText("success");
    }
}

void MainWindow::sort_char_up(MyList<char> *list) // сортировка списка char по возрастанию
{
    ui->textBrowserLog->insertPlainText("*try to sort up elements in List\n");
    list->sort_list(up);
    if (list->get_error() != "OK") // проверка ошибок функции класса MyList
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Warning");
        msgBox.setText(QString::fromStdString(list->get_error()));
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*List was sorted up\n");
        ui->textEditStatus->setText("success");
    }
}

void MainWindow::sort_int_down(MyList<int> *list) // сортировка списка int по убыванию
{
    ui->textBrowserLog->insertPlainText("*try to sort down elements in List\n");
    list->sort_list(down);
    if (list->get_error() != "OK") // проверка ошибок функции класса MyList
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Warning");
        msgBox.setText(QString::fromStdString(list->get_error()));
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*List was sorted down\n");
        ui->textEditStatus->setText("success");
    }
}

void MainWindow::sort_float_down(MyList<float> *list) // сортировка списка float  по убыванию
{
    ui->textBrowserLog->insertPlainText("*try to sort down elements in List\n");
    list->sort_list(down);
    if (list->get_error() != "OK") // проверка ошибок функции класса MyList
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Warning");
        msgBox.setText(QString::fromStdString(list->get_error()));
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*List was sorted down\n");
        ui->textEditStatus->setText("success");
    }
}

void MainWindow::sort_char_down(MyList<char> *list) // сортировка списка char  по убыванию
{
    ui->textBrowserLog->insertPlainText("*try to sort down elements in List\n");
    list->sort_list(down);
    if (list->get_error() != "OK") // проверка ошибок функции класса MyList
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Warning");
        msgBox.setText(QString::fromStdString(list->get_error()));
        msgBox.exec();
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(list->get_error()) + "\n");
        ui->textEditStatus->setText("error");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*List was sorted down\n");
        ui->textEditStatus->setText("error");
    }
}

void MainWindow::print_int(MyList<int> *list) // вывод на экран списка int
{
    ui->textBrowserLog->insertPlainText("*try to print List\n");
    string tempS=list->print_list_to_string();
    if (list->get_error() == "OK" || list->get_error() == "list is empty") // проверка ошибок функции класса MyList
    {
        ui->textBrowserList->setText(QString::fromStdString(tempS));
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(tempS));
        ui->textBrowserLog->insertPlainText("*List was printed\n");
        ui->textEditStatus->setText("success");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*Something wrong with List print\n");
        ui->textEditStatus->setText("error");
    }
}

void MainWindow::print_float(MyList<float> *list) // вывод на экран списка float
{
    ui->textBrowserLog->insertPlainText("*try to print List\n");
    string tempS=list->print_list_to_string();
    if (list->get_error() == "OK" || list->get_error() == "list is empty") // проверка ошибок функции класса MyList
    {
        ui->textBrowserList->setText(QString::fromStdString(tempS));
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(tempS));
        ui->textBrowserLog->insertPlainText("*List was printed\n");
        ui->textEditStatus->setText("success");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*Something wrong with List print\n");
        ui->textEditStatus->setText("error");
    }
}

void MainWindow::print_char(MyList<char> *list) // вывод на экран списка char
{
    ui->textBrowserLog->insertPlainText("*try to print List\n");
    string tempS=list->print_list_to_string();
    if (list->get_error() == "OK" || list->get_error() == "list is empty") // проверка ошибок функции класса MyList
    {
        ui->textBrowserList->setText(QString::fromStdString(tempS));
        ui->textBrowserLog->insertPlainText("*" + QString::fromStdString(tempS));
        ui->textBrowserLog->insertPlainText("*List was printed\n");
        ui->textEditStatus->setText("success");
    }
    else
    {
        ui->textBrowserLog->insertPlainText("*Something wrong with List print\n");
        ui->textEditStatus->setText("error");
    }
}

void MainWindow::on_pushButtonOK_clicked()
{
    if (ui->pushButtonAddElem->isHidden()) // начальный выбор типа списка произведен
    {
        ui->pushButtonAddElem->show();
        ui->pushButtonSortList->show();
        ui->pushButtonPrintList->show();
        ui->pushButtonSwapElems->show();
        ui->pushButtonDeleteElem->show();
        ui->comboBoxListOf->hide();
        ui->labelListof->raise();
        ui->textEditStatus->setText("success");
        switch(ui->comboBoxListOf->currentIndex())
        {
        case intList:
        {
            ui->labelListof->setText("List of int");
            ui->textBrowserLog->insertPlainText("*List of int is created\n");
            break;
        }
        case floatList:
        {
            ui->labelListof->setText("List of float");
            ui->textBrowserLog->insertPlainText("*List of float is created");
            break;
        }
        case charList:
        {
            ui->labelListof->setText("List of char");
            ui->textBrowserLog->insertPlainText("*List of char is created");
            break;
        }
        }
    }
    else switch (ToDo) // производится работа со списком
    {
    case addElem: // добавлени элемента в список
    {
        if (ui->comboBoxWhereAddElem->currentIndex() == beginningList) // добавление в начало списка
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                add_int_to_beginning(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                add_float_to_beginning(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                add_char_to_beginning(&listChar);
                break;
            }
            }
        }
        else if (ui->comboBoxWhereAddElem->currentIndex() == endList) // добавление в конец списка
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                add_int_to_end(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                add_float_to_end(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                add_char_to_end(&listChar);
                break;
            }
            }
        }
        else // добавление на позицию в списке
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                add_int_to_index(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                add_float_to_index(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                add_char_to_index(&listChar);
                break;
            }
            }
        }
        break;
    }
    case deleteElem: // удаление элемента из списка
    {
        if (ui->comboBoxWhereAddElem->currentIndex() == dataList) // удаление по данным
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                delete_int_data(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                delete_float_data(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                delete_char_data(&listChar);
                break;
            }
            }
        }
        else // удаление по позиции
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                delete_int_index(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                delete_float_index(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                delete_char_index(&listChar);
                break;
            }
            }
        }
        break;
    }
    case swapElems: // обмен мест элементов списка
    {
        if (ui->comboBoxWhereAddElem->currentIndex() == dataList) // обмен мест элементов по данным
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                swap_int_data(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                swap_float_data(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                swap_char_data(&listChar);
                break;
            }
            }
        }
        else // обмен мест элементов по позициям
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                swap_int_index(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                swap_float_index(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                swap_char_index(&listChar);
                break;
            }
            }
        }
        break;
    }
    case sortList: // сортировка списка
    {
        if (ui->comboBoxTypeSort->currentIndex() == up) // сортировка по возрастанию
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                sort_int_up(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                sort_float_up(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                sort_char_up(&listChar);
                break;
            }
            }
        }
        else // сортировка по убыванию
        {
            switch (ui->comboBoxListOf->currentIndex())
            {
            case intList: // для списка int
            {
                sort_int_down(&listInt);
                break;
            }
            case floatList: // для списка float
            {
                sort_float_down(&listFloat);
                break;
            }
            case charList: // для списка char
            {
                sort_char_down(&listChar);
                break;
            }
            }
        }
        break;
    }
    case printList: // вывод списка на экран
    {
        switch (ui->comboBoxListOf->currentIndex())
        {
        case intList: // для списка int
        {
            print_int(&listInt);
            break;
        }
        case floatList: // для списка float
        {
            print_float(&listFloat);
            break;
        }
        case charList: // для списка char
        {
            print_char(&listChar);
            break;
        }
        }
        break;
    }
    default:
    {
        ui->textEditStatus->setText("wait");
        QMessageBox msgBox;
        msgBox.setWindowTitle("Warning");
        msgBox.setText("Choose what to do with List");
        msgBox.exec();
    }
    }
}

void MainWindow::on_pushButtonAddElem_clicked()
{
    ui->FirstElem->show();
    ui->textEditDataFirstElem->show();
    ui->FirstElem->setText("data of add element");
    ui->TypeAddElem->show();
    ui->TypeAddElem->setText("add element to");
    ui->comboBoxWhereAddElem->show();
    ui->comboBoxWhereAddElem->clear();
    ui->comboBoxWhereAddElem->addItem("beginning", beginningList);
    ui->comboBoxWhereAddElem->addItem("end", endList);
    ui->comboBoxWhereAddElem->addItem("position", posList);
    ui->TypeSort->hide();
    ui->SecondElem->show();
    ui->SecondElem->setText("index of add element");
    ui->comboBoxTypeSort->hide();
    ui->textEditDataSecondElem->show();
    ui->List->hide();
    ui->scrollAreaList->hide();
    ui->textBrowserList->hide();
    ui->scrollAreaWidgetContentsList->hide();
    ui->textEditStatus->setText("wait");
    ToDo=addElem;
}

void MainWindow::on_pushButtonDeleteElem_clicked()
{
    ui->FirstElem->show();
    ui->textEditDataFirstElem->show();
    ui->FirstElem->setText("delete element");
    ui->TypeAddElem->setText("delete element by");
    ui->SecondElem->hide();
    ui->comboBoxTypeSort->hide();
    ui->textEditDataSecondElem->hide();
    ui->List->hide();
    ui->scrollAreaList->hide();
    ui->textBrowserList->hide();
    ui->scrollAreaWidgetContentsList->hide();
    ui->TypeAddElem->show();
    ui->comboBoxWhereAddElem->show();
    ui->comboBoxWhereAddElem->clear();
    ui->comboBoxWhereAddElem->addItem("data", dataList);
    ui->comboBoxWhereAddElem->addItem("index", indexList);
    ui->textEditStatus->setText("wait");
    ToDo=deleteElem;
}

void MainWindow::on_pushButtonSwapElems_clicked()
{
    ui->FirstElem->show();
    ui->textEditDataFirstElem->show();
    ui->FirstElem->setText("first element");
    ui->TypeAddElem->show();
    ui->TypeAddElem->setText("swap elements by");
    ui->SecondElem->show();
    ui->SecondElem->setText("second element");
    ui->textEditDataSecondElem->show();
    ui->comboBoxTypeSort->hide();
    ui->List->hide();
    ui->scrollAreaList->hide();
    ui->textBrowserList->hide();
    ui->scrollAreaWidgetContentsList->hide();
    ui->comboBoxWhereAddElem->show();
    ui->comboBoxWhereAddElem->clear();
    ui->comboBoxWhereAddElem->addItem("data", dataList);
    ui->comboBoxWhereAddElem->addItem("index", indexList);
    ui->textEditStatus->setText("wait");
    ToDo=swapElems;
}

void MainWindow::on_pushButtonSortList_clicked()
{
    ui->FirstElem->hide();
    ui->textEditDataFirstElem->hide();
    ui->SecondElem->hide();
    ui->textEditDataSecondElem->hide();
    ui->TypeSort->show();
    ui->comboBoxTypeSort->show();
    ui->List->hide();
    ui->scrollAreaList->hide();
    ui->textBrowserList->hide();
    ui->scrollAreaWidgetContentsList->hide();
    ui->TypeAddElem->hide();
    ui->comboBoxWhereAddElem->hide();
    ui->textEditStatus->setText("wait");
    ToDo=sortList;
}

void MainWindow::on_pushButtonPrintList_clicked()
{
    ui->FirstElem->hide();
    ui->textEditDataFirstElem->hide();
    ui->SecondElem->hide();
    ui->textEditDataSecondElem->hide();
    ui->TypeSort->hide();
    ui->comboBoxTypeSort->hide();
    ui->List->show();
    ui->scrollAreaList->show();
    ui->textBrowserList->show();
    ui->textBrowserList->clear();
    ui->scrollAreaWidgetContentsList->show();
    ui->TypeAddElem->hide();
    ui->comboBoxWhereAddElem->hide();
    ui->textEditStatus->setText("wait");
    ToDo=printList;

}
