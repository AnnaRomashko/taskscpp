#ifndef MYLIST_H
#define MYLIST_H

#include <iostream>
#include <stdio.h>
#include <string>

using namespace std;

enum TypeSort {up, down}; // виды сортировок списка
/* up - от меньшего к большему
 * down - от большего к меньшему
 */

template<typename ElemDataType>
struct MyListElem
{
    ElemDataType myListData;
    MyListElem *Right;
    MyListElem *Left;

    MyListElem()
    {
        int a=0;
        myListData=(ElemDataType)a;
        Right=nullptr;
        Left=nullptr;
    }

    MyListElem(ElemDataType data)
    {
        myListData=data;
        Right=nullptr;
        Left=nullptr;
    }
};

template<typename DataType>
class MyList
{
private:

    MyListElem<DataType> *head;
    MyListElem<DataType> *tail;
    int length;
    string error;

public:

    MyList();
    MyList(DataType data);
    MyListElem<DataType> *find_elem_by_data(DataType data); // найти элемент по данным
    MyListElem<DataType> *find_elem_by_index(int index); // найти элемент по номеру
    void add_elem_to_beginning(DataType data); // добавить элемент в начало списка
    void add_elem_to_end(DataType data); // добавить элемент в конец списка
    void add_elem_to_index(DataType data, int index); // добавить элемент на место номер index
    void delete_elem_by_data(DataType data); // удалить элемент из списка по данным
    void delete_elem_by_index(int index); // удалить элемент из списка по номеру
    void swap_elems_by_data(DataType data1, DataType data2); // поменять местами элементы в списке по данным
    void swap_elems_by_index(int index1, int index2); // поменять местами элементы в списке по номерам index
    void sort_list(TypeSort typeSort); // сортировать список
    void print_list(); // вывести на экран список
    string print_list_to_string(); //вывести список в строку
    string get_error(); // вывести переменную ошибок
};

#endif // MYLIST_H
