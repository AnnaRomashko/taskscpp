#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "mylist.h"

enum WhatToDo {addElem, deleteElem, swapElems, sortList, printList};

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButtonOK_clicked(); // кнопка подтверждения действия нажата

    void on_pushButtonAddElem_clicked(); // кнопка добавления элемента нажата

    void on_pushButtonDeleteElem_clicked(); // кнопка удаления элемента нажата

    void on_pushButtonSwapElems_clicked(); // кнопка обмена мест элементами нажата

    void on_pushButtonSortList_clicked(); // кнопка сортировки списка нажата

    void on_pushButtonPrintList_clicked(); // кнопка вывода списка нажата

private:
    Ui::MainWindow *ui;
    WhatToDo ToDo;
    MyList<int> listInt; // если создать их внутри функций, то они не видны, особенно если внутри if
    MyList<float> listFloat;
    MyList<char> listChar;

    void add_int_to_beginning(MyList<int> *list); // функции добавления элементов в список
    void add_int_to_end(MyList<int> *list);
    void add_int_to_index(MyList<int> *list);
    void add_float_to_beginning(MyList<float> *list);
    void add_float_to_end(MyList<float> *list);
    void add_float_to_index(MyList<float> *list);
    void add_char_to_beginning(MyList<char> *list);
    void add_char_to_end(MyList<char> *list);
    void add_char_to_index(MyList<char> *list);

    void delete_int_data(MyList<int> *list);  // функции удаления элемента из списка
    void delete_int_index(MyList<int> *list);
    void delete_float_data(MyList<float> *list);
    void delete_float_index(MyList<float> *list);
    void delete_char_data(MyList<char> *list);
    void delete_char_index(MyList<char> *list);

    void swap_int_data(MyList<int> *list);  // функции обмена мест элементов списка
    void swap_int_index(MyList<int> *list);
    void swap_float_data(MyList<float> *list);
    void swap_float_index(MyList<float> *list);
    void swap_char_data(MyList<char> *list);
    void swap_char_index(MyList<char> *list);

    void sort_int_up(MyList<int> *list);  // функции сортировки списка
    void sort_int_down(MyList<int> *list);
    void sort_float_up(MyList<float> *list);
    void sort_float_down(MyList<float> *list);
    void sort_char_up(MyList<char> *list);
    void sort_char_down(MyList<char> *list);

    void print_int(MyList<int> *list);   // функции вывода списка на экран
    void print_float(MyList<float> *list);
    void print_char(MyList<char> *list);
};
#endif // MAINWINDOW_H
